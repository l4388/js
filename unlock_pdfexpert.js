hostname = license.pdfexpert.com
#document
^https:\/\/license\.pdfexpert\.com\/api\/.*\/documents\/subscription\/(refresh$|check$) url script-response-body https://raw.githubusercontent.com/hermitli/sub/master/quan/JS/document.js


#Pdfexpert
^https:\/\/license\.pdfexpert\.com\/api\/1\.0\/pdfexpert6\/subscription\/(refresh$|check$) url script-response-body https://raw.githubusercontent.com/hermitli/sub/master/quan/JS/pdfexpert.js

^https:\/\/license\.pdfexpert\.com\/api\/2\.0\/pdfexpert6\/account\/attach_receipt url script-response-body https://raw.githubusercontent.com/hermitli/sub/master/quan/JS/pdfexpert.js

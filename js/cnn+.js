/*
{
  "subscriptions": [
    {
      "id": "450001090336458",
      "universalProductIndicator": "cnn_plus",
      "description": "CNN+ Monthly Subscription",
      "storefront": "iTunes",
      "storefrontInfo": {
        "storefrontId": "com.cnn.iphone",
        "storefrontType": "iTunes"
      },
      "nextBillDateMillis": 1649250858000,
      "cancelDateMillis": null,
      "purchaseDateMillis": 1648646059000,
      "active": true,
      "sku": "cnn_plus_monthly_ios",
      "trialing": true,
      "paymentType": null,
      "price": 5.99,
      "billingPeriod": {
        "count": 1,
        "interval": "month"
      },
      "card": null,
      "terminationDateMillis": null,
      "currency": "USD",
      "status": "Trialing"
    }
  ]
}

*/
var obj = JSON.parse($response.body);
obj["subscriptions"]["description"] = "CNN+ Yearly Subscription";
obj["subscriptions"]["nextBillDateMillis"] = 2649250858000;
obj["subscriptions"]["sku"] = "cnn_plus_yearly_ios";
$done({body:JSON.stringify(obj)});

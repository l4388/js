const  header = $request.headers;

if (header["User-Agent"].indexOf(Currency) != -1){
let obj = JSON.parse($response.body);
obj["subscriber"]["subscriptions"]= {
      "com.jeffreygrossman.currencyapp.iap.plus": {
        "is_sandbox": false,
        "ownership_type" : "PURCHASED",
        "billing_issues_detected_at": null,
        "period_type" : "trial",
        "expires_date": "2099-12-01T03:51:32Z",
        "grace_period_expires_date" : null,
        "unsubscribe_detected_at" : null,
        "original_purchase_date" : "2021-12-23T01:56:13Z",
        "purchase_date" : "2021-12-23T01:56:12Z",
        "store" : "app_store"
      }
    };
obj["subscriber"]["entitlements"]= {
      "plus": {
        "grace_period_expires_date" : null,
        "purchase_date" : "2021-12-23T01:56:12Z",
        "product_identifier" : "com.jeffreygrossman.currencyapp.iap.plus",
        "expires_date" : "2099-12-01T03:51:32Z"
      }
    };
$done({body: JSON.stringify(obj)});
}

const path1 = "/user_info.json";
const path2 = "/user_center.json";

const url = $request.url;
var obj = JSON.parse($response.body);

if (url.indexOf(path1) != -1){
obj["response"] = {
    "status": 1,
    "ad_vip_type": 4,
    "period_type": 3,
    "agreement_platform": "1",
    "expire_time": "2099-04-14 00:00:00",
    "permission": [
    ],
    "discount_status": 64,
    "agreement_status": 2,
    "type": 3,
    "product_type": 2,
    "expire_date": "2099-04-14"
  }
}

if (url.indexOf(path2) != -1){
obj["response"]["user_info"] = {
    "status": 1,
    "period_type": 3,
    "discount_status": 64,
    "agreement_status": 3,
    "product_type": 2,
    "expire_date": "2099-04-14"
  }
}

/*obj=  {
  "status": 0,
  "expires_date": "9999-06-06 19:57:41"
};*/

$done({body: JSON.stringify(obj)});
